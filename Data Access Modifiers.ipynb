{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d728cccb",
   "metadata": {},
   "source": [
    "## Data Access Modifiers in Python\n",
    "A common feature of many programming languages, particularly languages students are likely to encounter in university courses, is the idea of “private” scope in classes. This refers to the fact that a class can have internal state that is only visible to an instance of that class. In C++ this looks like:\n",
    "\n",
    "```c++\n",
    "class Foo\n",
    "{\n",
    "  public:\n",
    "    Foo();\n",
    "    ~Foo();\n",
    "    void bar();\n",
    "\n",
    "  private:\n",
    "    void buf();\n",
    "    int baz;\n",
    "};\n",
    "```\n",
    "\n",
    "Understanding this is pretty straightforward. The methods Foo(), ~Foo(), and bar() are public, callable by any other part of the program. buf() and baz are not accesible to parts of the program that are not the object itself.\n",
    "\n",
    "Python, however, does not have scope restrictions like this. The same class as in the above example would look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e23dc26",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Foo:\n",
    "    def __init__(self):\n",
    "        self.baz = 0\n",
    "    \n",
    "    def bar():\n",
    "        pass\n",
    "\n",
    "    def buf():\n",
    "        pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e28ed957",
   "metadata": {},
   "source": [
    "No access modifiers, everything is public! However, a common convention in Python is prefixing private methods or attributes with an underscore, which would look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "880b878c",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Foo:\n",
    "    def __init__(self):\n",
    "        self._baz = 0\n",
    "    \n",
    "    def bar():\n",
    "        pass\n",
    "\n",
    "    def _buf():\n",
    "        pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ab512e0",
   "metadata": {},
   "source": [
    "By convention, `_foo` means that the foo attribute is an internal method or field, and should not be used by programmers. Some resources describe this as “making” something private, which is incorrect! Observe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc771235",
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = Foo()\n",
    "foo._baz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70ccf2c4",
   "metadata": {},
   "source": [
    "One other thing I have seen in resources for new developers is that a double underscore makes something “super private”. This is… not true. But it *looks* true:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06248f1a",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Foo:\n",
    "    def __init__(self):\n",
    "        self.__baz = 0\n",
    "        \n",
    "    def bar():\n",
    "        pass\n",
    "    \n",
    "    def _buf():\n",
    "        pass\n",
    "    \n",
    "foo = Foo()\n",
    "foo.__baz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08852522",
   "metadata": {},
   "source": [
    "What happened? Turning to the vars method, we see this output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2914691c",
   "metadata": {},
   "outputs": [],
   "source": [
    "vars(foo)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4cf31049",
   "metadata": {},
   "source": [
    "So the attribute is there, just… renamed? Mangled, almost? In fact, what’s happening is called [name mangling](https://docs.python.org/3/tutorial/classes.html#private-variables). The main idea of name mangling is that a subclass of Foo might implement an attribute also called `_baz`, and then you’re in trouble for deciding which one should be used. In fact, Python will simply use the subclass variable, and override the parent class field:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3635d3f5",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Foo:\n",
    "    def __init__(self):\n",
    "        self._baz = 0\n",
    "    \n",
    "    def bar():\n",
    "        pass\n",
    "\n",
    "    def _buf():\n",
    "        pass\n",
    "    \n",
    "class SubFoo(Foo):\n",
    "    def __init__(self):\n",
    "        self._baz = 1\n",
    "        \n",
    "sub_foo = SubFoo()\n",
    "sub_foo._baz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "454db50d",
   "metadata": {},
   "source": [
    "What the double underscore is actually doing is exactly what it appears to be doing: rewriting the name of the method so that it won’t conflict with a subclass. You can still access the supposedly super private parts of a class with no problems at all, if you happen to know the name mangling scheme:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b995f5cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "foo._Foo__baz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a401bb2a",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "And thus we see that Python doesn't really have data access modifiers like C++ or other languages. This is important, beause the lack of true access modifiers lowers the barrier to start using nominally private parst of the API represented by the class functions."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
