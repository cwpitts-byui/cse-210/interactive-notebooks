{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "dad5e4a6",
   "metadata": {},
   "source": [
    "# Polymorphism\n",
    "Polymorphism has two main characteristics. Quoting from the [Microsoft C# docs](https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/object-oriented/polymorphism):\n",
    "\n",
    "> At run time, objects of a derived class may be treated as objects of a base class in places such as method parameters and collections or arrays. When this polymorphism occurs, the object's declared type is no longer identical to its run-time type.\n",
    "Base classes may define and implement virtual methods, and derived classes can override them, which means they provide their own definition and implementation. At run-time, when client code calls the method, the CLR looks up the run-time type of the object, and invokes that override of the virtual method. In your source code you can call a method on a base class, and cause a derived class's version of the method to be executed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "104fb371",
   "metadata": {},
   "source": [
    "## Run-time types\n",
    "Here is an example of declared versus run-time types. Consider this base class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bef0bf8b",
   "metadata": {},
   "outputs": [],
   "source": [
    "from abc import ABC, abstractmethod\n",
    "from pprint import pprint\n",
    "from typing import List, Dict\n",
    "\n",
    "\n",
    "class Penguin(ABC):\n",
    "    \"\"\"A simple penguin model class.\n",
    "    \n",
    "    The Penguin class is an abstract class that provides an\n",
    "    interface for subtypes.\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self, feeds_on: str, appetite: int):\n",
    "        \"\"\"Penguin.\n",
    "        \n",
    "        Args:\n",
    "          feeds_on (str): The type of fish a penguin feeds on\n",
    "          appetite (int): How much of a particular fish a penguin eats\n",
    "\n",
    "        \"\"\"\n",
    "        self._feeds_on: str = feeds_on\n",
    "        self._appetite: int = appetite\n",
    "\n",
    "    @abstractmethod\n",
    "    def feed(self, environment: Dict[str, int]):\n",
    "        \"\"\"Consume food in the environment.\n",
    "        \n",
    "        Args:\n",
    "          environment (Dict[str, int]): The current environment\n",
    "\n",
    "        \"\"\"\n",
    "        raise NotImplementedError\n",
    "\n",
    "    def waddle(self):\n",
    "        \"\"\"Waddle around looking all cute and stuff.\n",
    "        \"\"\"\n",
    "        print(\"Waddling!\")\n",
    "\n",
    "    @property\n",
    "    def feeds_on(self) -> str:\n",
    "        \"\"\"The type of fish a penguin feeds on.\n",
    "        \n",
    "        Returns:\n",
    "          str: The value of the self._feeds_on attribute\n",
    "        \"\"\"\n",
    "        return self._feeds_on"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad22258c",
   "metadata": {},
   "source": [
    "This class is a generic base class, or an *abstract class*. Abstract classes are used to define *interfaces* that code can expect to be implemented in any derived class.\n",
    "\n",
    "Consider this simple environment model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ca30837",
   "metadata": {},
   "outputs": [],
   "source": [
    "environment: Dict[str, int] = {\n",
    "    \"small_fish\": 120,\n",
    "    \"midsize_fish\": 100,\n",
    "    \"large_fish\": 30,\n",
    "}\n",
    "pprint(environment)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e8ae6d1",
   "metadata": {},
   "source": [
    "Let's implement some derived classes of `Penguin` so we can interact with this environment:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "89c8b48b",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Emperor(Penguin):\n",
    "    \"\"\"Emperor penguins are the largest species of Pengiuns.\n",
    "    \n",
    "    This penguin mostly eats large fish, but will eat smaller\n",
    "    fish if no larger fish are to be had.\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self, appetite: int):\n",
    "        \"\"\"Emperor.\n",
    "\n",
    "        Args:\n",
    "          appetite (int): How much this particular penguin eats\n",
    "\n",
    "        \"\"\"\n",
    "        self._also_feeds_on: str = \"midsize_fish\"\n",
    "        super().__init__(\"large_fish\", appetite)\n",
    "\n",
    "    def feed(self, environment: Dict[str, int]):\n",
    "        \"\"\"Consume food in the environment.\n",
    "        \n",
    "        Args:\n",
    "          environment (Dict[str, int]): The current environment\n",
    "\n",
    "        \"\"\"\n",
    "        if environment.get(self._feeds_on, 0) >= self._appetite:\n",
    "            environment[self._feeds_on] -= self._appetite\n",
    "        elif environment.get(self._also_feeds_on, 0) >= self._appetite * 2:\n",
    "            environment[self._also_feeds_on] -= self._appetite * 2\n",
    "\n",
    "\n",
    "class Adelie(Penguin):\n",
    "    \"\"\"Adelie pengiuins mate for life.\n",
    "    \n",
    "    This penguin only eats small fish.\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self, appetite: int):\n",
    "        \"\"\"Adelie.\n",
    "        \n",
    "        Args:\n",
    "          appetite (int): How much this penguin eats\n",
    "\n",
    "        \"\"\"\n",
    "        super().__init__(\"small_fish\", appetite)\n",
    "\n",
    "    def feed(self, environment: Dict[str, int]):\n",
    "        \"\"\"Consume food in the environment.\n",
    "        \n",
    "        Args:\n",
    "          environment (Dict[str, int]): The current environment\n",
    "\n",
    "        \"\"\"\n",
    "        if environment.get(self._feeds_on, 0) >= self._appetite:\n",
    "            environment[self._feeds_on] -= self._appetite"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85f58ed8",
   "metadata": {},
   "source": [
    "Notice that the implementation of the `feed` method is different in each subclass. This is where being able to have a different run-time type is useful; without that, we would always call the original base class method, which just throws an error (this is a common paradigm to make sure that someone actually implementes the required methods).\n",
    "\n",
    "Now, let's build a collection of penguins to use in interacting with the environment:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b8aea22",
   "metadata": {},
   "outputs": [],
   "source": [
    "penguins: List[Penguin] = []"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "056a1d9e",
   "metadata": {},
   "source": [
    "Notice that the type of this list is declared as `Penguin`. Python doesn't actually have static typing as this annotation might imply, but from a semantic point of view, this is a list of `Penguin` instances. Let's add some penguins to this list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25a8ffba",
   "metadata": {},
   "outputs": [],
   "source": [
    "penguins.append(Emperor(appetite=15))\n",
    "penguins.append(Adelie(appetite=10))\n",
    "penguins.append(Emperor(appetite=20))\n",
    "pprint(penguins)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "599a44ca",
   "metadata": {},
   "source": [
    "Now let's iterate over the list of penguins and have them feed in the environment:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e83a130b",
   "metadata": {},
   "outputs": [],
   "source": [
    "for penguin in penguins:\n",
    "    penguin.feed(environment)\n",
    "\n",
    "pprint(environment)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9dce41e3",
   "metadata": {},
   "source": [
    "Notice that we didn't have to specify which type of `Penguin` we were dealing with inside of the `for` loop. Because they were all subtypes that implemented the interface represented by the base class, the run-time type was different than the nominally declared type, but we were able to run without error. This guarantee of substitutability is the property known as the [Liskov Substitution Principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle), first proposed by Barbra Liskov. One major difference in how Python handles objects and inheritance is that Python is capable of introspection, and the actual types are known. In C++, for example, declaring a vector of pointers to `Penguins` like this:\n",
    "\n",
    "```c++\n",
    "std::vector<Penguin*> penguins;\n",
    "```\n",
    "\n",
    "Results in a container where you cannot know the subtype, and may only access the types and methods declared in the base class. In Python, you can know each object's type in the `penguins` list, despite the nominal type declaration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1060af0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "for penguin in penguins:\n",
    "    print(type(penguin))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d522282",
   "metadata": {},
   "source": [
    "## Summary\n",
    "To summarize, polymorphism's ability to have a run-time type that is a specialization of the declared type is useful because it allows for specialized behavior in derived classes to be handled generically by one particular routine, permitting an engineer to write a single function instead of one specialized function for each subtype. This is done via inheritance and the implementation of the interface represented by the abstract base class."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
