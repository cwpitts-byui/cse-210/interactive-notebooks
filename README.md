# Interactive Notebooks

Some interactive Python notebooks demonstrating various concepts from
the course.

## Requirements
Jupyter and Python3 are required for these notebooks. You can also run
them on MyBinder:

* Equality [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cwpitts-byui%2Fcse-210%2Finteractive-notebooks/HEAD?labpath=Python%20Equality.ipynb)
* Polymorphism [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cwpitts-byui%2Fcse-210%2Finteractive-notebooks/HEAD?labpath=Polymorphism.ipynb)
* Data Access Modifiers [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cwpitts-byui%2Fcse-210%2Finteractive-notebooks/HEAD?labpath=Data%20Access%20Modifiers.ipynb)
